﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    class Caretaker
    {
        public Memento Memento { get;  set; }

        public Caretaker(Memento memento)
        {
            this.Memento = memento;
        }
    }
}
