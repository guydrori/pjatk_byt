﻿namespace Memento
{
    partial class MainDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTextBox = new System.Windows.Forms.TextBox();
            this.SaveStateButton = new System.Windows.Forms.Button();
            this.RestoreStateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MainTextBox
            // 
            this.MainTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainTextBox.Location = new System.Drawing.Point(0, 0);
            this.MainTextBox.Multiline = true;
            this.MainTextBox.Name = "MainTextBox";
            this.MainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MainTextBox.Size = new System.Drawing.Size(474, 243);
            this.MainTextBox.TabIndex = 0;
            this.MainTextBox.TextChanged += new System.EventHandler(this.OnTextChanged);
            // 
            // SaveStateButton
            // 
            this.SaveStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveStateButton.Location = new System.Drawing.Point(12, 259);
            this.SaveStateButton.Name = "SaveStateButton";
            this.SaveStateButton.Size = new System.Drawing.Size(107, 27);
            this.SaveStateButton.TabIndex = 1;
            this.SaveStateButton.Text = "Save State";
            this.SaveStateButton.UseVisualStyleBackColor = true;
            this.SaveStateButton.Click += new System.EventHandler(this.OnButtonClick);
            // 
            // RestoreStateButton
            // 
            this.RestoreStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestoreStateButton.Location = new System.Drawing.Point(136, 259);
            this.RestoreStateButton.Name = "RestoreStateButton";
            this.RestoreStateButton.Size = new System.Drawing.Size(150, 27);
            this.RestoreStateButton.TabIndex = 2;
            this.RestoreStateButton.Text = "Restore State";
            this.RestoreStateButton.UseVisualStyleBackColor = true;
            this.RestoreStateButton.Click += new System.EventHandler(this.OnButtonClick);
            // 
            // MainDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 298);
            this.Controls.Add(this.RestoreStateButton);
            this.Controls.Add(this.SaveStateButton);
            this.Controls.Add(this.MainTextBox);
            this.Name = "MainDialog";
            this.Text = "Guy\'s Memento based text editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MainTextBox;
        private System.Windows.Forms.Button SaveStateButton;
        private System.Windows.Forms.Button RestoreStateButton;
    }
}

