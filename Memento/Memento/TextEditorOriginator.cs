﻿namespace Memento
{
    class TextEditorOriginator
    {
        public string State { get; set; }

        public Memento CreateMemento()
        {
            return new Memento(this.State);
        }

        public void SetMemento(Memento memento)
        {
            State = memento.State;
        }
    }
}
