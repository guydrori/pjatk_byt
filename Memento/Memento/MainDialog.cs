﻿using System;
using System.Windows.Forms;

namespace Memento
{
    public partial class MainDialog : Form
    {
        private TextEditorOriginator _originator;
        private Caretaker _stateCaretaker;
        public MainDialog()
        {
            InitializeComponent();
            _originator = new TextEditorOriginator();
            _originator.State = MainTextBox.Text;
            _stateCaretaker = new Caretaker(_originator.CreateMemento());

        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            if (sender == SaveStateButton)
            {
                _stateCaretaker.Memento = _originator.CreateMemento();
                MessageBox.Show("State saved successfully", "Guy's Memento based text editor");
            } else if (sender == RestoreStateButton)
            {
                _originator.SetMemento(_stateCaretaker.Memento);
                MainTextBox.Text = _originator.State;
                MessageBox.Show("State restored successfully", "Guy's Memento based text editor");
            }
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            if (sender == MainTextBox)
            {
                _originator.State = MainTextBox.Text;
            }
        }
    }
}
