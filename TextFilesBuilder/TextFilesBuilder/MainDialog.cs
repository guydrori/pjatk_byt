﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextFilesBuilder.Builders;

namespace TextFilesBuilder
{
    public partial class MainDialog : Form
    {
        private Director director = new Director();
        private HTMLBuilder htmlBuilder = new HTMLBuilder();
        private DOCXBuilder docxBuilder = new DOCXBuilder();
        public MainDialog()
        {
            InitializeComponent();
        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            if (sender == SaveHTMLButton)
            {
                if (MainTextBoxContainsText())
                {
                    SaveFileDialog saveHTMLDialog = new SaveFileDialog();
                    saveHTMLDialog.Filter = "HTML file|*.html";
                    saveHTMLDialog.Title = "Save HTML";
                    saveHTMLDialog.ShowDialog();
                    if (saveHTMLDialog.FileName != "")
                    {
                        director.Construct(htmlBuilder, MainTextBox.Text);
                        FileContents fileContents = htmlBuilder.GetResult();
                        File.WriteAllBytes(saveHTMLDialog.FileName, fileContents.ByteArray);
                        MessageBox.Show("HTML file saved successfully", "Guy's Builder Example");
                    }
                }
            } else if (sender == SaveDOCXButton)
            {
                if (MainTextBoxContainsText())
                {
                    SaveFileDialog saveDOCXDialog = new SaveFileDialog();
                    saveDOCXDialog.Filter = "Word 2007-2016 document|*.docx";
                    saveDOCXDialog.Title = "Save DOCX";
                    saveDOCXDialog.ShowDialog();
                    if (saveDOCXDialog.FileName != "")
                    {
                        director.Construct(docxBuilder, MainTextBox.Text);
                        FileContents fileContents = docxBuilder.GetResult();
                        File.WriteAllBytes(saveDOCXDialog.FileName, fileContents.ByteArray);
                        MessageBox.Show("DOCX file saved successfully", "Guy's Builder Example");
                    }
                }
            }
        }

        private bool MainTextBoxContainsText()
        {
            if (MainTextBox.TextLength > 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show("The text box cannot be empty!", "Error");
                return false;
            }
        }
    }
}
