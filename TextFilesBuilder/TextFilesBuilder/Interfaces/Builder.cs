﻿namespace TextFilesBuilder.Interfaces
{
    public interface Builder
    {
        void AddHeader();
        void AddText(string contents);
        void AddFooter();
        FileContents GetResult();
    }
}
