﻿using Novacode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextFilesBuilder.Interfaces;

namespace TextFilesBuilder.Builders
{
    class DOCXBuilder : Builder
    {
        private MemoryStream _stream = new MemoryStream();
        private DocX _document;

        public DOCXBuilder()
        {
            _document = DocX.Create(_stream);
        }
        public void AddHeader()
        {
            _document.AddHeaders();
            _document.Headers.odd.InsertParagraph().Append(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")).Alignment = Alignment.right;
        }

        public void AddText(string contents)
        {
            using (StringReader reader = new StringReader(contents))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Paragraph p = _document.InsertParagraph();
                    p.Append(line).FontSize(12).Font(new System.Drawing.FontFamily("Calibri"));
                }
            }
        }

        public void AddFooter()
        {
            _document.AddFooters();
            _document.Footers.odd.InsertParagraph().Append("DOCXBuilder").Alignment = Alignment.center;
        }


        public FileContents GetResult()
        {
            _document.Save();
            FileContents result = new FileContents(_stream.ToArray());
            _document.Dispose();
            return result;
        }
    }
}
