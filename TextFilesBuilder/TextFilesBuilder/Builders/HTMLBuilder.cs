﻿using System;
using System.IO;
using System.Text;
using TextFilesBuilder.Interfaces;

namespace TextFilesBuilder.Builders
{
    class HTMLBuilder : Builder
    {
        private static readonly string HTML_HEADER_START = "<!DOCTYPE HTML>"+ Environment.NewLine + "<html>" +Environment.NewLine + "\t<head>" + Environment.NewLine + "\t\t<meta charset=\"utf-8\">" + Environment.NewLine;
        private static readonly string HTML_HEADER_END = "\t</head>" + Environment.NewLine  + "\t<body>\n";
        private static readonly string HTML_FOOTER = "\n\t</body>" + Environment.NewLine + "</html>";
        private string fileContents = "";
    
        public void AddHeader()
        {
            fileContents += HTML_HEADER_START;
            string titleString = "\t\t<title>Document -" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</title>" + Environment.NewLine;
            fileContents += titleString;
            fileContents += HTML_HEADER_END;
        }

        public void AddText(string contents)
        {
            using (StringReader reader = new StringReader(contents))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    fileContents += "\t\t<p>\n\t\t\t";
                    fileContents += line;
                    fileContents += "\n\t\t</p>";
                }
            }
        }

        public void AddFooter()
        {
            fileContents += HTML_FOOTER;
        }

        public FileContents GetResult()
        {
            return new FileContents(Encoding.UTF8.GetBytes(fileContents));
        }
    }
}
