﻿namespace TextFilesBuilder
{
    partial class MainDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTextBox = new System.Windows.Forms.TextBox();
            this.SaveHTMLButton = new System.Windows.Forms.Button();
            this.SaveDOCXButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MainTextBox
            // 
            this.MainTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainTextBox.Location = new System.Drawing.Point(0, 0);
            this.MainTextBox.Multiline = true;
            this.MainTextBox.Name = "MainTextBox";
            this.MainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MainTextBox.Size = new System.Drawing.Size(445, 261);
            this.MainTextBox.TabIndex = 0;
            // 
            // SaveHTMLButton
            // 
            this.SaveHTMLButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveHTMLButton.Location = new System.Drawing.Point(12, 267);
            this.SaveHTMLButton.Name = "SaveHTMLButton";
            this.SaveHTMLButton.Size = new System.Drawing.Size(161, 35);
            this.SaveHTMLButton.TabIndex = 1;
            this.SaveHTMLButton.Text = "Save Text as HTML";
            this.SaveHTMLButton.UseVisualStyleBackColor = true;
            this.SaveHTMLButton.Click += new System.EventHandler(this.OnButtonClick);
            // 
            // SaveDOCXButton
            // 
            this.SaveDOCXButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveDOCXButton.Location = new System.Drawing.Point(179, 267);
            this.SaveDOCXButton.Name = "SaveDOCXButton";
            this.SaveDOCXButton.Size = new System.Drawing.Size(161, 35);
            this.SaveDOCXButton.TabIndex = 2;
            this.SaveDOCXButton.Text = "Save Text as DOCX";
            this.SaveDOCXButton.UseVisualStyleBackColor = true;
            this.SaveDOCXButton.Click += new System.EventHandler(this.OnButtonClick);
            // 
            // MainDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 310);
            this.Controls.Add(this.SaveDOCXButton);
            this.Controls.Add(this.SaveHTMLButton);
            this.Controls.Add(this.MainTextBox);
            this.Name = "MainDialog";
            this.Text = "Guy\'s Builder Example";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MainTextBox;
        private System.Windows.Forms.Button SaveHTMLButton;
        private System.Windows.Forms.Button SaveDOCXButton;
    }
}

