﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextFilesBuilder.Interfaces;

namespace TextFilesBuilder
{
    class Director
    {
        public void Construct(Builder builder, string contents)
        {
            builder.AddHeader();
            builder.AddText(contents);
            builder.AddFooter();
        }
    }
}
