﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFilesBuilder
{
    public class FileContents
    {
        public byte[] ByteArray { get; private set; }

        public FileContents(byte[] binaryContents)
        {
            ByteArray = binaryContents;
        }
    }
}
