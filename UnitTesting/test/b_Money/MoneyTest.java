package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MoneyTest {
	Currency SEK, DKK, NOK, EUR;
	Money SEK100, EUR10, SEK200, EUR20, SEK0, EUR0, SEKn100;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
		SEK100 = new Money(10000, SEK);
		EUR10 = new Money(1000, EUR);
		SEK200 = new Money(20000, SEK);
		EUR20 = new Money(2000, EUR);
		SEK0 = new Money(0, SEK);
		EUR0 = new Money(0, EUR);
		SEKn100 = new Money(-10000, SEK);
	}

	/**
	 * Checking for all of the sample objects whether getAmount() returns the same value they were initialized with.
	 */
	@Test
	public void testGetAmount() {
		assertEquals(10000,(int)SEK100.getAmount());
		assertEquals(1000,(int)EUR10.getAmount());
		assertEquals(20000,(int)SEK200.getAmount());
		assertEquals(2000,(int)EUR20.getAmount());
		assertEquals(0,(int)SEK0.getAmount());
		assertEquals(0,(int)EUR0.getAmount());
		assertEquals(-10000,(int)SEKn100.getAmount());
	}
	
	/**
	 * Checking for one example of each currency whether the getCurrency() returns the appropriate currency.
	 * The returned currency's name and rate is manually compared to those defined in the setUp() method due to a lack of an implementation
	 * of equals() in the Currency class
	 */
	@Test
	public void testGetCurrency() {
		//fail("Write test case here");
		assertEquals(SEK.getName(),SEK100.getCurrency().getName());
		assertEquals(SEK.getRate(),SEK100.getCurrency().getRate(),0.001);
		assertEquals(EUR.getName(),EUR10.getCurrency().getName());
		assertEquals(EUR.getRate(),EUR10.getCurrency().getRate(),0.001);
	}
	/**
	 * Checking for each example whether it returns the appropriate string based on it's amount and currency
	 */
	@Test
	public void testToString() {
		assertEquals("100.00 SEK",SEK100.toString());
		assertEquals("10.00 EUR",EUR10.toString());
		assertEquals("200.00 SEK",SEK200.toString());
		assertEquals("20.00 EUR",EUR20.toString());
		assertEquals("0.00 SEK",SEK0.toString());
		assertEquals("0.00 EUR",EUR0.toString());
		assertEquals("-100.00 SEK",SEKn100.toString());
	}
	/**
	 * Checking for each example whether the global value is calculated correctly
	 */
	@Test
	public void testGlobalValue() {
		assertEquals("The global value for SEK100 was calculated incorrectly",1500,(int)SEK100.getCurrency().universalValue(SEK100.getAmount()));
		assertEquals("The global value for EUR10 was calculated incorrectly",1500,(int)EUR10.getCurrency().universalValue(EUR10.getAmount()));
		assertEquals("The global value for SEK200 was calculated incorrectly",3000,(int)SEK200.getCurrency().universalValue(SEK200.getAmount()));
		assertEquals("The global value for EUR20 was calculated incorrectly",3000,(int)EUR20.getCurrency().universalValue(EUR20.getAmount()));
		assertEquals("The global value for SEK0 was calculated incorrectly",0,(int)SEK0.getCurrency().universalValue(SEK0.getAmount()));
		assertEquals("The global value for EUR0 was calculated incorrectly",0,(int)EUR0.getCurrency().universalValue(EUR0.getAmount()));
		assertEquals("The global value for SEKn100 was calculated incorrectly",-1500,(int)SEKn100.getCurrency().universalValue(SEKn100.getAmount()));
	}
	/**
	 * Checking whether instances with the same global value or the same value are treated as equal
	 * by comparing various combinations of instances
	 */
	@Test
	public void testEqualsMoney() {
		assertTrue(SEK100.equals(EUR10));
		assertFalse(SEK100.equals(SEK200));
		assertTrue(SEK100.equals(EUR10));
		assertFalse(SEKn100.equals(SEK100));
		assertTrue(SEK200.equals(EUR20));
		assertTrue(EUR10.equals(EUR10));
		assertTrue(EUR0.equals(SEK0));
		assertTrue(EUR20.equals(EUR20));
	}
	
	/**
	 * Checking the result of adding 50 to each instance from the setUp() method
	 * by comparing it to the manually calculated result
	 */
	@Test
	public void testAdd() {
		//fail("Write test case here");
		Money SEK50 = new Money(5000,SEK);
		Money EUR50 = new Money(5000,EUR);
		assertTrue(SEK100.add(SEK50).equals(new Money(15000,SEK)));
		assertTrue(EUR10.add(EUR50).equals(new Money(6000,EUR)));
		assertTrue(SEK200.add(SEK50).equals(new Money(25000,SEK)));
		assertTrue(EUR20.add(EUR50).equals(new Money(7000,EUR)));
		assertTrue(SEK0.add(SEK50).equals(SEK50));
		assertTrue(EUR0.add(EUR50).equals(EUR50));
		assertTrue(SEKn100.add(SEK50).equals(new Money(-5000,SEK)));
	}

	/**
	 * Checking the result of subtracting 50 from each instance from the setUp() method
	 * by comparing it to the manually calculated result
	 */
	@Test
	public void testSub() {
		//fail("Write test case here");
		Money SEK50 = new Money(5000,SEK);
		Money EUR50 = new Money(5000,EUR);
		assertTrue(SEK100.sub(SEK50).equals(new Money(5000,SEK)));
		assertTrue(EUR10.sub(EUR50).equals(new Money(-4000,EUR)));
		assertTrue(SEK200.sub(SEK50).equals(new Money(15000,SEK)));
		assertTrue(EUR20.sub(EUR50).equals(new Money(-3000,EUR)));
		assertTrue(SEK0.sub(SEK50).equals(new Money(-5000,SEK)));
		assertTrue(EUR0.sub(EUR50).equals(new Money(-5000,EUR)));
		assertTrue(SEKn100.sub(SEK50).equals(new Money(-15000,SEK)));
		
	}
	
	/**Checking whether the Money instances representing 0 are classified as 0.
	 * 
	 */
	@Test
	public void testIsZero() {
		//fail("Write test case here");
		assertTrue(SEK0.isZero());
		assertTrue(EUR0.isZero());
		assertFalse(SEK100.isZero());
	}
	
	/**
	 * Checking whether SEKn100 is the negation of SEK100 in addition to a counter example
	 */
	@Test
	public void testNegate() {
		assertTrue(SEKn100.equals(SEK100.negate()));
		assertTrue(SEK100.equals(SEKn100.negate()));
		assertFalse(SEK100.equals(SEK100.negate()));
	}
	
	/**
	 * Checking whether compareTo by showing one or more examples for each case.
	 */
	@Test
	public void testCompareTo() {
		//fail("Write test case here");
		assertTrue(SEK200.compareTo(SEK100) > 0);
		assertTrue(EUR10.compareTo(EUR20) < 0);
		assertTrue(EUR0.compareTo(SEK0) == 0);
		assertTrue(SEK100.compareTo(EUR10) == 0);
	}
}
