package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CurrencyTest {
	Currency SEK, DKK, NOK, EUR;
	
	@Before
	public void setUp() throws Exception {
		/* Setup currencies with exchange rates */
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
	}

	/** Checking if getName returns the correct name for the currency it is called on. 
	 */
	@Test
	public void testGetName() {
		//fail("Write test case here");
		assertEquals("SEK name is returned incorrectly",SEK.getName(),"SEK");
		assertEquals("DKK name is returned incorrectly",DKK.getName(),"DKK");
		assertEquals("EUR name is returned incorrectly",EUR.getName(),"EUR");
	}
	
	/** Checking whether getRate returns the correct rate for the currency on which it is invoked.
	 */
	@Test
	public void testGetRate() {
		assertEquals("SEK rate is returned incorrectly",new Double(0.15),SEK.getRate());
		assertEquals("DKK rate is returned incorrectly",new Double(0.20),DKK.getRate());
		assertEquals("EUR rate is returned incorrectly",new Double(1.5),EUR.getRate());
	}
	
	/** Checking whether we can successfully change the rate for a currency by setting it,
	 * and then checking whether getRate() returns the newly set rate. 
	 */
	@Test
	public void testSetRate() {
		//fail("Write test case here");
		SEK.setRate(0.20);
		assertEquals("SEK rate is changed incorrectly",new Double(0.20),SEK.getRate());
		DKK.setRate(0.60);
		assertEquals("DKK rate is returned incorrectly",new Double(0.60),DKK.getRate());
		EUR.setRate(3.3);
		assertEquals("EUR rate is returned incorrectly",new Double(3.3),EUR.getRate());
	}
	
	/**Checking whether the universal value is calculated correctly by comparing the result
	 * of an invocation for a sample input value with the manually calculated correct result according to the currencies' rates.
	 */
	@Test
	public void testGlobalValue() {
		assertEquals("SEK global value is calculated incorrectly",new Integer(450),SEK.universalValue(3000));
		assertEquals("DKK global value is calculated incorrectly",new Integer(600),DKK.universalValue(3000));
		assertEquals("EUR global value is calculated incorrectly",new Integer(300),EUR.universalValue(200));
	}
	
	/** Checking whether values are correctly converted between currencies by checking whether the conversion works in both directions
	 * (does the converted value return the original value if converted to the original currency?)
	 */
	@Test
	public void testValueInThisCurrency() {
		assertEquals("SEK value from DKK is calculated incorrectly",new Integer(133),SEK.valueInThisCurrency(100, DKK));
		assertEquals("SEK value from EUR is calculated incorrectly", new Integer(5000),SEK.valueInThisCurrency(500, EUR));
		assertEquals("DKK value from SEK is calculated incorrectly", new Integer(100),DKK.valueInThisCurrency(133, SEK));
		assertEquals("DKK value from EUR is calculated incorrectly", new Integer(3750),DKK.valueInThisCurrency(500, EUR));
		assertEquals("EUR value from DKK is calculated incorrectly", new Integer(500),EUR.valueInThisCurrency(5000, SEK));
		assertEquals("EUR value from DKK is calculated incorrectly", new Integer(500), EUR.valueInThisCurrency(3750, DKK));
	}

}
