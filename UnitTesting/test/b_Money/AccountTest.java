package b_Money;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

import static org.junit.Assert.*;

public class AccountTest {
	Currency SEK, DKK;
	Bank Nordea;
	Bank DanskeBank;
	Bank SweBank;
	Account testAccount;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		SweBank.openAccount("Alice");
		testAccount = new Account("Hans", SEK);
		testAccount.deposit(new Money(10000000, SEK));

		SweBank.deposit("Alice", new Money(1000000, SEK));
	}
	
	/**
	 * Testing adding and removing a timed payment for the test account
	 * by adding, then checking if it exists, before removing and verifying removal.
	 */
	@Test
	public void testAddRemoveTimedPayment() {
		//fail("Write test case here");
		testAccount.addTimedPayment("1", 30, 5, new Money(50000,SEK), SweBank, "Alice");
		assertTrue(testAccount.timedPaymentExists("1"));
		testAccount.removeTimedPayment("1");
		assertFalse(testAccount.timedPaymentExists("1"));
		
	}
	/**
	 * Performing a test timed payment between the testAccount and Alice's account
	 * for the initial number of ticks (the next value)
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		//fail("Write test case here");
		testAccount.addTimedPayment("1", 30, 5, new Money(50000,SEK), SweBank, "Alice");
		for (int i = 0; i < 5; i++) {
			testAccount.tick();
		}
		assertTrue("The transfer amount hasn't been withdrawn from testAccount",testAccount.getBalance().equals(new Money(9950000,SEK)));
		assertTrue("Alice hasn't received the money",SweBank.getBalance("Alice").equals(1050000));
	}

	/**
	 * Testing and verifying whether depositing and withdrawing works on the testAccount using a predefined value.
	 */
	@Test
	public void testAddWithdraw() {
		testAccount.deposit(new Money(30000,SEK));
		assertTrue("The money was not deposited into the test account",testAccount.getBalance().equals(new Money(10030000,SEK)));
		testAccount.withdraw(new Money(15000,SEK));
		assertTrue("The money has not been withdrawn from the test account",testAccount.getBalance().equals(new Money(10015000,SEK)));
	}
	
	/**
	 * Checking whether the balance in the test account is in accordance with the balance defined in the setUp() method
	 */
	@Test
	public void testGetBalance() {
		//fail("Write test case here");
		assertTrue(testAccount.getBalance().toString(),new Money(10000000,SEK).equals(testAccount.getBalance()));
	}
}
