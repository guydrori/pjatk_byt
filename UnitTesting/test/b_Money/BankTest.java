package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BankTest {
	Currency SEK, DKK;
	Bank SweBank, Nordea, DanskeBank;
	
	@Before
	public void setUp() throws Exception {
		DKK = new Currency("DKK", 0.20);
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		Nordea = new Bank("Nordea", SEK);
		DanskeBank = new Bank("DanskeBank", DKK);
		SweBank.openAccount("Ulrika");
		SweBank.openAccount("Bob");
		Nordea.openAccount("Bob");
		DanskeBank.openAccount("Gertrud");
	}
	
	/**
	 * Checking whether getName() returns the name defined in the constructor for each bank in the setUp() method
	 */
	@Test
	public void testGetName() {
		//fail("Write test case here");
		assertEquals("SweBank",SweBank.getName());
		assertEquals("Nordea",Nordea.getName());
		assertEquals("DanskeBank",DanskeBank.getName());
	}
	/**
	 * Checking whether getCurrency() returns the correct currency as defined in the constructor for each bank in the example.
	 * For each currency the name and rate will be compared due to the fact Currency doesn't have an equals() method.
	 */
	@Test
	public void testGetCurrency() {
		//fail("Write test case here");
		assertEquals(SEK.getName(),SweBank.getCurrency().getName());
		assertEquals(SEK.getRate(),(double)SweBank.getCurrency().getRate(),0.0001);
		assertEquals(SEK.getName(),Nordea.getCurrency().getName());
		assertEquals(SEK.getRate(),(double)Nordea.getCurrency().getRate(),0.0001);
		assertEquals(DKK.getName(),DanskeBank.getCurrency().getName());
		assertEquals(DKK.getRate(),(double)DanskeBank.getCurrency().getRate(),0.0001);
	}

	/**
	 * Testing the opening of an account for each bank by creating an account and then checking if it exists using getBalance().
	 * @throws AccountExistsException
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testOpenAccount() throws AccountExistsException, AccountDoesNotExistException {
		//fail("Write test case here");
		SweBank.openAccount("Guy");
		assertEquals("Guy's account doesn't exist",0,(int)SweBank.getBalance("Guy"));
		Nordea.openAccount("Jonathan");
		assertEquals("Jonathan's account doesn't exist",0,(int)Nordea.getBalance("Jonathan"));
		try {
			SweBank.openAccount("Guy");
			fail("Account created when an account with this identifier exists already");
		} catch (AccountExistsException e) {
			assertTrue(e.getClass() == AccountExistsException.class);
		}
	}
	
	/**
	 * For each account from the setUp() method, the balance will be checked after depositing a fixed amount
	 * under the assumption that the accounts initially have a balance of 0.
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testDeposit() throws AccountDoesNotExistException {
		//fail("Write test case here");
		SweBank.deposit("Ulrika", new Money(50000,SEK)); //Depositing 500 SEK into Ulrika's account
		SweBank.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in SweBank
		Nordea.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in Nordea
		DanskeBank.deposit("Gertrud", new Money(50000,DKK)); //Depositing 500 DKK into Gertrud's account
		assertEquals("Ulrika's balance is not equal to 500 SEK",50000,(int)SweBank.getBalance("Ulrika"));
		assertEquals("Bob's balance in SweBank is not equal to 500",50000,(int)SweBank.getBalance("Bob"));
		assertEquals("Bob's balance in Nordea is not equal to 500",50000,(int)Nordea.getBalance("Bob"));
		assertEquals("Gertrud's balance is not equal to 500",50000,(int)DanskeBank.getBalance("Gertrud"));
		try { //Attempt to deposit into a non-existing account
			SweBank.deposit("Guy",new Money(50000,SEK));
			fail("Deposit suceeded for a non-existing account");
		} catch (AccountDoesNotExistException e) {
			assertTrue(e.getClass() == AccountDoesNotExistException.class);
		}
	}
	
	/**
	 * Testing withdraw() for each account from the setUp() method, with varios amounts, to check whether it will work for negative balances.
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testWithdraw() throws AccountDoesNotExistException {
		//fail("Write test case here");
		SweBank.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in SweBank
		Nordea.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in Nordea
		DanskeBank.deposit("Gertrud", new Money(50000,DKK)); //Depositing 500 DKK into Gertrud's account
		SweBank.withdraw("Ulrika",new Money(5000,SEK));
		SweBank.withdraw("Bob",new Money(20000,SEK));
		Nordea.withdraw("Bob",new Money(60000,SEK));
		DanskeBank.withdraw("Gertrud", new Money(5000,DKK));
		assertEquals("Ulrika's balance is not equal to -50 SEK",-5000,(int)SweBank.getBalance("Ulrika"));
		assertEquals("Bob's balance in SweBank is not equal to 300",30000,(int)SweBank.getBalance("Bob"));
		assertEquals("Bob's balance in Nordea is not equal to -100",-10000,(int)Nordea.getBalance("Bob"));
		assertEquals("Gertrud's balance is not equal to 300",45000,(int)DanskeBank.getBalance("Gertrud"));
		try { //Attempt to withdraw from a non-existing account
			SweBank.withdraw("Guy",new Money(5000,SEK));
			fail("Withdraw succeeded for a non-existing account");
		} catch (AccountDoesNotExistException e) {
			assertTrue(e.getClass() == AccountDoesNotExistException.class);
		}
	}
	/**
	 * For each account defined the setUp() method, getBalance() will be checked after depositing various amounts,
	 * or on a balance of zero.
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testGetBalance() throws AccountDoesNotExistException {
		//fail("Write test case here");
		assertEquals("Ulrika's balance is not equal to 0",0,(int)SweBank.getBalance("Ulrika"));
		SweBank.deposit("Bob", new Money(10000,SEK)); //Depositing 100 SEK into Bob's account in SweBank
		assertEquals("Bob's balance in SweBank is not equal to 100",10000,(int)SweBank.getBalance("Bob"));
		Nordea.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in Nordea
		assertEquals("Bob's balance in Nordea is not equal to 500",50000,(int)Nordea.getBalance("Bob"));
		DanskeBank.deposit("Gertrud", new Money(30000,DKK)); //Depositing 300 DKK into Gertrud's account
		assertEquals("Gertrud's balance is not equal to 300",30000,(int)DanskeBank.getBalance("Gertrud"));
	}
	/**
	 * Testing the transfer functionality by performing multiple transfers, inter-bank and intra-bank
	 * and checking whether the balances changed accordingly
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testTransfer() throws AccountDoesNotExistException {
		//fail("Write test case here");
		SweBank.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in SweBank
		SweBank.deposit("Ulrika", new Money(50000,SEK)); //Depositing 500 SEK into Ulrika's account
		SweBank.transfer("Bob", "Ulrika", new Money(20000,SEK)); //Transferring 200 SEK from Bob to Ulrika
		Nordea.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in Nordea
		DanskeBank.deposit("Gertrud", new Money(50000,DKK)); //Depositing 500 DKK into Gertrud's account
		Nordea.transfer("Bob", DanskeBank, "Gertrud",new Money(10000,SEK)); //Transferring 100 SEK from Bob's account to Gertrud's account
		assertEquals("Bob's balance in SweBank is not equal 300 SEK",30000,(int)SweBank.getBalance("Bob"));
		assertEquals("Ulrika's balance is not equal 700 SEK",70000,(int)SweBank.getBalance("Ulrika"));
		assertEquals("Bob's balance in Nordea is not equal 400 SEK",40000,(int)Nordea.getBalance("Bob"));
		assertEquals("Gertrud's balance is not equal 575 DKK",57500,(int)DanskeBank.getBalance("Gertrud"));
		try {
			SweBank.transfer("Ulrika", "Alice", new Money(5000,SEK));
			fail("Transfer to a non-existing account suceeded");
		} catch (Exception e) {
			assertTrue(e.getClass() == AccountDoesNotExistException.class);
		}
	}
	/**
	 * Testing the timed payment functionality by testing one timed payment in the same bank and one between banks
	 * and then checking whether the accounts' balances have changed accordingly
	 * @throws AccountDoesNotExistException
	 */
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		//fail("Write test case here");
		SweBank.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in SweBank
		SweBank.deposit("Ulrika", new Money(50000,SEK)); //Depositing 500 SEK into Ulrika's account
		//Defining a timed payment for 100 SEK between Bob and Ulrika
		SweBank.addTimedPayment("Bob", "U1", 30, 5, new Money(10000,SEK), SweBank, "Ulrika"); 
		for (int i = 0; i < 5; i++) {
			SweBank.tick(); //Testing the passing of time for this payment
		}
		Nordea.deposit("Bob", new Money(50000,SEK)); //Depositing 500 SEK into Bob's account in Nordea
		DanskeBank.deposit("Gertrud", new Money(50000,DKK)); //Depositing 500 DKK into Gertrud's account
		Nordea.addTimedPayment("Bob", "B1", 60, 5, new Money(5000,SEK), DanskeBank, "Gertrud");
		for (int i = 0; i < 5; i++) {
			Nordea.tick(); //Testing the passing of time for this payment
		}
		assertEquals("Bob's balance in SweBank is not equal 400 SEK",40000,(int)SweBank.getBalance("Bob"));
		assertEquals("Ulrika's balance is not equal 600 SEK",60000,(int)SweBank.getBalance("Ulrika"));
		assertEquals("Bob's balance in Nordea is not equal 450 SEK",45000,(int)Nordea.getBalance("Bob"));
		assertEquals("Gertrud's balance is not equal 537.50 DKK",53750,(int)DanskeBank.getBalance("Gertrud"));
		SweBank.addTimedPayment("Ulrika","A1",90,2,new Money(5000,SEK),SweBank,"Alice");
		for (int i = 0; i < 2; i++) {
			SweBank.tick(); //Testing the passing of time for this payment
		}
		assertEquals("Ulrika transferred money to a non-existing account",60000,(int)SweBank.getBalance("Ulrika"));
	}
}
