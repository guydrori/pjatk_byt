package eu.drori.byt.task9;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;

import eu.drori.byt.task9.observers.Observer;
import eu.drori.task9.memento.Memento;

public class URL {
	private java.net.URL _url;
	private ArrayList<Observer> _observers = new ArrayList<>();
	private static ArrayList<URL> _monitoredURLs = new ArrayList<>();
	private long _lastModificationTime;
	private Thread _monitoringThread;
	
	public URL (String urlString) throws MalformedURLException {
		_url = new java.net.URL(urlString);
		_monitoringThread = new Thread(()-> {
			while (true) {
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) _url.openConnection();
					long lastModificationTime = conn.getLastModified();
					if (_lastModificationTime != lastModificationTime) {
						_lastModificationTime = lastModificationTime;
						notifyObservers();
					}
					conn.disconnect();
					Thread.sleep(60000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		_monitoringThread.start();
	}
	
	public URL (java.net.URL url)  {
		_url = url;
		_monitoringThread = new Thread(()-> {
			while (true) {
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) _url.openConnection();
					long lastModificationTime = conn.getLastModified();
					if (_lastModificationTime != lastModificationTime) {
						_lastModificationTime = lastModificationTime;
						notifyObservers();
					}
					conn.disconnect();
					Thread.sleep(60000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		_monitoringThread.start();
	}
	
	private void notifyObservers() {
		for (Observer o: _observers) {
			o.update();
		}
	}
	
	public void addObserver(Observer observer) {
		if (!_monitoredURLs.contains(this)) _monitoredURLs.add(this);
		_observers.add(observer);
	}
	
	public boolean removeObserver(Observer observer) {
		if (_observers.size() == 0 && _monitoredURLs.contains(this)) _monitoredURLs.remove(this); 
		return _observers.remove(observer);
	}
	
	public static Memento CreateMemento() {
		return new Memento(_monitoredURLs);
	}
		
	public static void SetMemento(Memento memento) {
		_monitoredURLs = memento.getState();
	}
	
	@Override
	public String toString() {
		return _url.toString();
	}
	
	public Date getLastModificationDate() {
		return new Date(_lastModificationTime);
	}
	
	public long getLastModificationTimeStamp() {
		return _lastModificationTime;
	}
	
	public void setLastModificationTimeStamp(long timeStamp) {
		_lastModificationTime = timeStamp;
	}
	
}
