package eu.drori.byt.task9.observers;

import eu.drori.byt.task9.URL;

public class URLObserver implements Observer {
	private URL _url;
	
	public URLObserver(URL url) {
		_url = url;
		_url.addObserver(this);
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println(_url + " modified on " + _url.getLastModificationDate().toString());
	}

}
