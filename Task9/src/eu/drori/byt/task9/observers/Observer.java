package eu.drori.byt.task9.observers;

public interface Observer {
	public void update();
}
