package eu.drori.task9.memento;

import java.util.ArrayList;

import eu.drori.byt.task9.URL;

public class Memento {
	private ArrayList<URL> _state;
	
	public Memento(ArrayList<URL> state) {
		_state = state;
	}
	
	public ArrayList<URL> getState() {
		return _state;
	}
	
	public void setState(ArrayList<URL> state) {
		_state = state;
	}
}
