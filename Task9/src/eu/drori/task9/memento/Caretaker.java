package eu.drori.task9.memento;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import eu.drori.byt.task9.URL;
import eu.drori.byt.task9.observers.URLObserver;

public class Caretaker {
	private Memento _memento;
	
	public Caretaker() {
		//Do nothing
	}
	
	public Caretaker(Memento memento) {
		_memento = memento;
	}
	
	public Memento getMemento() {
		return _memento;
	}
	
	public void setMemento(Memento memento) {
		_memento = memento;
		saveMementoToFile("data.dat");
	}
	
	private void saveMementoToFile(String filename) {
		List<String> lines = _memento.getState().stream()
				.map(u->u.toString() + " " + u.getLastModificationTimeStamp()).collect(Collectors.toList());
		Path filePath = Paths.get(filename);
		try {
			Files.write(filePath, lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Memento loadMementoFromFile(String filename) {
		try {
			Path filePath = Paths.get(filename);
			List<String> lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
			ArrayList<URL> urlList = lines.stream().map(l-> {
				String[] words = l.split("\\s+");
				URL url = null;
				try {
					url = new URL(words[0]);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				url.setLastModificationTimeStamp(Long.parseLong(words[1]));
				new URLObserver(url);
				return url;
			}).collect(Collectors.toCollection(ArrayList::new));
			_memento = new Memento(urlList);
			return _memento;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void restoreStateFromFile(String filename) {
		URL.SetMemento(loadMementoFromFile(filename));
	}
}
