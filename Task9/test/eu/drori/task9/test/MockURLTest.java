package eu.drori.task9.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.drori.byt.task9.URL;
import eu.drori.byt.task9.observers.Observer;
import eu.drori.byt.task9.observers.URLObserver;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class MockURLTest {
	
	private final ByteArrayOutputStream _outputContent = new ByteArrayOutputStream();
	private HttpURLConnection _conn;
	private URLStreamHandler _handler;
	
	@Before
	public void setUp() {
		_conn = Mockito.mock(java.net.HttpURLConnection.class);
		when(_conn.getLastModified()).thenReturn((long)1513208344*1000);
		doNothing().when(_conn).disconnect();
		_handler = new URLStreamHandler() {
			
			@Override
			protected URLConnection openConnection(java.net.URL u) throws IOException {
				// TODO Auto-generated method stub
				return _conn;
			}
		};
		System.setOut(new PrintStream(_outputContent));
	}
	
	@After
	public void after() {
		System.setOut(null);
	}
	
	@Test
	public void getLastModified() throws MalformedURLException {
		URL url = new URL(new java.net.URL("http","test.com",80,"",_handler));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals((long)1513208344*1000, url.getLastModificationTimeStamp());
	}
	
	@Test
	public void notifyObservers() throws MalformedURLException {
		Observer observer = Mockito.mock(Observer.class);
		URL url = new URL(new java.net.URL("http","test.com",80,"",_handler));
		url.addObserver(observer);
		verify(observer).update();
	}
	
	@Test
	public void start() throws MalformedURLException {
		URL url = new URL(new java.net.URL("http","test.com",80,"",_handler));
		new URLObserver(url);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue("No change notification printed to console!", _outputContent.toString().contains("http://test.com:80 modified on Thu Dec 14 00:39:04 CET 2017"));
		assertEquals((long)1513208344*1000, url.getLastModificationTimeStamp());
		when(_conn.getLastModified()).thenReturn((long)1513209910*1000);
		try {
			Thread.sleep(61000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue("No change notification printed to console!", _outputContent.toString().contains("http://test.com:80 modified on Thu Dec 14 01:05:10 CET 2017"));
		assertEquals((long)1513209910*1000, url.getLastModificationTimeStamp());
	}
}
