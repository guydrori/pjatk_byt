package eu.drori.task9.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.drori.byt.task9.URL;
import eu.drori.byt.task9.observers.URLObserver;
import eu.drori.task9.memento.Caretaker;
import eu.drori.task9.memento.Memento;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MementoTest {
	private final ByteArrayOutputStream _outputContents = new ByteArrayOutputStream();
	private Caretaker _caretaker;
	
	@Before
	public void before() {
		_caretaker = new Caretaker();
		System.setOut(new PrintStream(_outputContents));
	}
	
	@After
	public void after() {
		System.setOut(null);
	}
	
	@Test
	public void saveStateToFile() throws IOException {
		HttpURLConnection conn = Mockito.mock(java.net.HttpURLConnection.class);
		when(conn.getLastModified()).thenReturn((long)1513208344*1000);
		doNothing().when(conn).disconnect();
		URLStreamHandler handler = new URLStreamHandler() {
			
			@Override
			protected URLConnection openConnection(java.net.URL u) throws IOException {
				// TODO Auto-generated method stub
				return conn;
			}
		};
		URL url = new URL(new java.net.URL("http","test.com",80,"",handler));
		new URLObserver(url);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals((long)1513208344*1000, url.getLastModificationTimeStamp());
		_caretaker.setMemento(URL.CreateMemento());
		Path dataFilePath = Paths.get("data.dat");
		List<String> fileLineList = Files.readAllLines(dataFilePath, StandardCharsets.UTF_8);
		assertEquals(fileLineList.get(0), "http://test.com:80 " + (long)1513208344*1000);
	}
	
	@Test
	public void loadMementoFromFile() throws InterruptedException {
		Memento memento = _caretaker.loadMementoFromFile("data.dat");
		assertEquals((long)1513208344*1000, memento.getState().get(0).getLastModificationTimeStamp());
		assertEquals("http://test.com:80",memento.getState().get(0).toString());
	}
	
}
