﻿using ChainOfResponsibility.Handlers;

namespace ChainOfResponsibility.Interfaces
{
    public abstract class Handler
    {
        public static readonly Handler FirstHandler = new AdditionHandler();
        public abstract int HandleRequest(Request request);
    }
}
