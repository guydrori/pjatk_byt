﻿namespace ChainOfResponsibility
{
    partial class MainDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.Parameter2UpDown = new System.Windows.Forms.NumericUpDown();
            this.Parameter1UpDown = new System.Windows.Forms.NumericUpDown();
            this.Parameter2Label = new System.Windows.Forms.Label();
            this.Parameter1Label = new System.Windows.Forms.Label();
            this.OperationChoiceLabel = new System.Windows.Forms.Label();
            this.RadioButtonPanel = new System.Windows.Forms.Panel();
            this.DivisionRadioButton = new System.Windows.Forms.RadioButton();
            this.MultiplicationRadioButton = new System.Windows.Forms.RadioButton();
            this.SubtractionRadioButton = new System.Windows.Forms.RadioButton();
            this.AdditionRadioButton = new System.Windows.Forms.RadioButton();
            this.CalculateButton = new System.Windows.Forms.Button();
            this.ResultGroupBox = new System.Windows.Forms.GroupBox();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.ExponentRadioButton = new System.Windows.Forms.RadioButton();
            this.ParametersGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter2UpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter1UpDown)).BeginInit();
            this.RadioButtonPanel.SuspendLayout();
            this.ResultGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ParametersGroupBox
            // 
            this.ParametersGroupBox.Controls.Add(this.Parameter2UpDown);
            this.ParametersGroupBox.Controls.Add(this.Parameter1UpDown);
            this.ParametersGroupBox.Controls.Add(this.Parameter2Label);
            this.ParametersGroupBox.Controls.Add(this.Parameter1Label);
            this.ParametersGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ParametersGroupBox.Location = new System.Drawing.Point(12, 13);
            this.ParametersGroupBox.Name = "ParametersGroupBox";
            this.ParametersGroupBox.Size = new System.Drawing.Size(297, 100);
            this.ParametersGroupBox.TabIndex = 0;
            this.ParametersGroupBox.TabStop = false;
            this.ParametersGroupBox.Text = "Parameters";
            // 
            // Parameter2UpDown
            // 
            this.Parameter2UpDown.Location = new System.Drawing.Point(188, 40);
            this.Parameter2UpDown.Name = "Parameter2UpDown";
            this.Parameter2UpDown.Size = new System.Drawing.Size(64, 23);
            this.Parameter2UpDown.TabIndex = 3;
            // 
            // Parameter1UpDown
            // 
            this.Parameter1UpDown.Location = new System.Drawing.Point(41, 40);
            this.Parameter1UpDown.Name = "Parameter1UpDown";
            this.Parameter1UpDown.Size = new System.Drawing.Size(64, 23);
            this.Parameter1UpDown.TabIndex = 2;
            // 
            // Parameter2Label
            // 
            this.Parameter2Label.AutoSize = true;
            this.Parameter2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Parameter2Label.Location = new System.Drawing.Point(139, 40);
            this.Parameter2Label.Name = "Parameter2Label";
            this.Parameter2Label.Size = new System.Drawing.Size(22, 20);
            this.Parameter2Label.TabIndex = 1;
            this.Parameter2Label.Text = "2:";
            // 
            // Parameter1Label
            // 
            this.Parameter1Label.AutoSize = true;
            this.Parameter1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Parameter1Label.Location = new System.Drawing.Point(17, 40);
            this.Parameter1Label.Name = "Parameter1Label";
            this.Parameter1Label.Size = new System.Drawing.Size(22, 20);
            this.Parameter1Label.TabIndex = 0;
            this.Parameter1Label.Text = "1:";
            // 
            // OperationChoiceLabel
            // 
            this.OperationChoiceLabel.AutoSize = true;
            this.OperationChoiceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OperationChoiceLabel.Location = new System.Drawing.Point(12, 116);
            this.OperationChoiceLabel.Name = "OperationChoiceLabel";
            this.OperationChoiceLabel.Size = new System.Drawing.Size(161, 20);
            this.OperationChoiceLabel.TabIndex = 1;
            this.OperationChoiceLabel.Text = "Choose an operation:";
            // 
            // RadioButtonPanel
            // 
            this.RadioButtonPanel.Controls.Add(this.ExponentRadioButton);
            this.RadioButtonPanel.Controls.Add(this.DivisionRadioButton);
            this.RadioButtonPanel.Controls.Add(this.MultiplicationRadioButton);
            this.RadioButtonPanel.Controls.Add(this.SubtractionRadioButton);
            this.RadioButtonPanel.Controls.Add(this.AdditionRadioButton);
            this.RadioButtonPanel.Location = new System.Drawing.Point(16, 140);
            this.RadioButtonPanel.Name = "RadioButtonPanel";
            this.RadioButtonPanel.Size = new System.Drawing.Size(157, 127);
            this.RadioButtonPanel.TabIndex = 2;
            // 
            // DivisionRadioButton
            // 
            this.DivisionRadioButton.AutoSize = true;
            this.DivisionRadioButton.Location = new System.Drawing.Point(17, 85);
            this.DivisionRadioButton.Name = "DivisionRadioButton";
            this.DivisionRadioButton.Size = new System.Drawing.Size(62, 17);
            this.DivisionRadioButton.TabIndex = 3;
            this.DivisionRadioButton.TabStop = true;
            this.DivisionRadioButton.Text = "Division";
            this.DivisionRadioButton.UseVisualStyleBackColor = true;
            // 
            // MultiplicationRadioButton
            // 
            this.MultiplicationRadioButton.AutoSize = true;
            this.MultiplicationRadioButton.Location = new System.Drawing.Point(17, 62);
            this.MultiplicationRadioButton.Name = "MultiplicationRadioButton";
            this.MultiplicationRadioButton.Size = new System.Drawing.Size(86, 17);
            this.MultiplicationRadioButton.TabIndex = 2;
            this.MultiplicationRadioButton.TabStop = true;
            this.MultiplicationRadioButton.Text = "Multiplication";
            this.MultiplicationRadioButton.UseVisualStyleBackColor = true;
            // 
            // SubtractionRadioButton
            // 
            this.SubtractionRadioButton.AutoSize = true;
            this.SubtractionRadioButton.Location = new System.Drawing.Point(17, 39);
            this.SubtractionRadioButton.Name = "SubtractionRadioButton";
            this.SubtractionRadioButton.Size = new System.Drawing.Size(79, 17);
            this.SubtractionRadioButton.TabIndex = 1;
            this.SubtractionRadioButton.TabStop = true;
            this.SubtractionRadioButton.Text = "Subtraction";
            this.SubtractionRadioButton.UseVisualStyleBackColor = true;
            // 
            // AdditionRadioButton
            // 
            this.AdditionRadioButton.AutoSize = true;
            this.AdditionRadioButton.Location = new System.Drawing.Point(17, 16);
            this.AdditionRadioButton.Name = "AdditionRadioButton";
            this.AdditionRadioButton.Size = new System.Drawing.Size(63, 17);
            this.AdditionRadioButton.TabIndex = 0;
            this.AdditionRadioButton.TabStop = true;
            this.AdditionRadioButton.Text = "Addition";
            this.AdditionRadioButton.UseVisualStyleBackColor = true;
            // 
            // CalculateButton
            // 
            this.CalculateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CalculateButton.Location = new System.Drawing.Point(200, 192);
            this.CalculateButton.Name = "CalculateButton";
            this.CalculateButton.Size = new System.Drawing.Size(109, 40);
            this.CalculateButton.TabIndex = 3;
            this.CalculateButton.Text = "Calculate";
            this.CalculateButton.UseVisualStyleBackColor = true;
            this.CalculateButton.Click += new System.EventHandler(this.OnButtonClick);
            // 
            // ResultGroupBox
            // 
            this.ResultGroupBox.Controls.Add(this.ResultLabel);
            this.ResultGroupBox.Location = new System.Drawing.Point(200, 119);
            this.ResultGroupBox.Name = "ResultGroupBox";
            this.ResultGroupBox.Size = new System.Drawing.Size(109, 67);
            this.ResultGroupBox.TabIndex = 4;
            this.ResultGroupBox.TabStop = false;
            this.ResultGroupBox.Text = "Result";
            // 
            // ResultLabel
            // 
            this.ResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ResultLabel.Location = new System.Drawing.Point(0, 16);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(109, 48);
            this.ResultLabel.TabIndex = 0;
            this.ResultLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // ExponentRadioButton
            // 
            this.ExponentRadioButton.AutoSize = true;
            this.ExponentRadioButton.Location = new System.Drawing.Point(17, 107);
            this.ExponentRadioButton.Name = "ExponentRadioButton";
            this.ExponentRadioButton.Size = new System.Drawing.Size(70, 17);
            this.ExponentRadioButton.TabIndex = 4;
            this.ExponentRadioButton.TabStop = true;
            this.ExponentRadioButton.Text = "Exponent";
            this.ExponentRadioButton.UseVisualStyleBackColor = true;
            // 
            // MainDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 270);
            this.Controls.Add(this.ResultGroupBox);
            this.Controls.Add(this.CalculateButton);
            this.Controls.Add(this.RadioButtonPanel);
            this.Controls.Add(this.OperationChoiceLabel);
            this.Controls.Add(this.ParametersGroupBox);
            this.Name = "MainDialog";
            this.Text = "Guy\'s Calculator";
            this.ParametersGroupBox.ResumeLayout(false);
            this.ParametersGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter2UpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter1UpDown)).EndInit();
            this.RadioButtonPanel.ResumeLayout(false);
            this.RadioButtonPanel.PerformLayout();
            this.ResultGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ParametersGroupBox;
        private System.Windows.Forms.NumericUpDown Parameter2UpDown;
        private System.Windows.Forms.NumericUpDown Parameter1UpDown;
        private System.Windows.Forms.Label Parameter2Label;
        private System.Windows.Forms.Label Parameter1Label;
        private System.Windows.Forms.Label OperationChoiceLabel;
        private System.Windows.Forms.Panel RadioButtonPanel;
        private System.Windows.Forms.RadioButton DivisionRadioButton;
        private System.Windows.Forms.RadioButton MultiplicationRadioButton;
        private System.Windows.Forms.RadioButton SubtractionRadioButton;
        private System.Windows.Forms.RadioButton AdditionRadioButton;
        private System.Windows.Forms.Button CalculateButton;
        private System.Windows.Forms.GroupBox ResultGroupBox;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.RadioButton ExponentRadioButton;
    }
}

