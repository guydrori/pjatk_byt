﻿using System;
using ChainOfResponsibility.Interfaces;

namespace ChainOfResponsibility.Handlers
{
    class AdditionHandler: Handler
    {
        private static readonly Handler successor = new MultiplicationHandler();

        override public int HandleRequest(Request request)
        {
            if (request.Type == Request.Types.ADD)
            {
                return request.Parameter1 + request.Parameter2;
            } else
            {
                return successor.HandleRequest(request);
            }
        }
    }
}
