﻿using ChainOfResponsibility.Interfaces;

namespace ChainOfResponsibility.Handlers
{
    class MultiplicationHandler: Handler
    {
        private static readonly Handler successor = new DivisionHandler();

        override public int HandleRequest(Request request)
        {
            if (request.Type == Request.Types.MULTIPLY)
            {
                return request.Parameter1 * request.Parameter2;
            }
            else
            {
                return successor.HandleRequest(request);
            }
        }
    }
}
