﻿using ChainOfResponsibility.Interfaces;
using System;

namespace ChainOfResponsibility.Handlers
{
    class SubtractionHandler : Handler
    {
        private static readonly Handler successor = new ExponentHandler();
        override public int HandleRequest(Request request)
        {
            if (request.Type == Request.Types.EXPONENT)
            {
                return Convert.ToInt32(Math.Pow(request.Parameter1, request.Parameter2));
            }
            else
            {
                return successor.HandleRequest(request);
            }
        }
    }
}
