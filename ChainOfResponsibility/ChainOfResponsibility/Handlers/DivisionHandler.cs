﻿using ChainOfResponsibility.Interfaces;
using System;

namespace ChainOfResponsibility.Handlers
{
    class DivisionHandler : Handler
    {
        private static readonly Handler successor = new SubtractionHandler();

        override public int HandleRequest(Request request)
        {
            if (request.Type == Request.Types.DIVIDE)
            {
                if (request.Parameter2 != 0)
                {
                    return request.Parameter1 / request.Parameter2;
                } else
                {
                    throw new ArithmeticException("Cannot divide by 0!");
                }
            }
            else
            {
                return successor.HandleRequest(request);
            }
        }
    }
}
