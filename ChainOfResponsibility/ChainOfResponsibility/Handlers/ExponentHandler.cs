﻿using ChainOfResponsibility.Interfaces;
using System;

namespace ChainOfResponsibility.Handlers
{
    public class ExponentHandler: Handler
    {
        override public int HandleRequest(Request request)
        {
            if (request.Type == Request.Types.SUBTRACT)
            {
                return request.Parameter1 - request.Parameter2;
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
}
