﻿using ChainOfResponsibility.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChainOfResponsibility
{
    public partial class MainDialog : Form
    {


        public MainDialog()
        {
            InitializeComponent();
            AdditionRadioButton.Checked = true;
        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            if (sender == CalculateButton)
            {
               try
                {
                    int parameter1 = Convert.ToInt32(Parameter1UpDown.Value);
                    int parameter2 = Convert.ToInt32(Parameter2UpDown.Value);
                    int result = Handler.FirstHandler.HandleRequest(new Request(parameter1, parameter2, GetChosenRequestType()));
                    ResultLabel.Text = result.ToString();
                } catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private Request.Types GetChosenRequestType()
        {
            if (AdditionRadioButton.Checked)
            {
                return Request.Types.ADD;
            } else if (SubtractionRadioButton.Checked)
            {
                return Request.Types.SUBTRACT;
            } else if (MultiplicationRadioButton.Checked)
            {
                return Request.Types.MULTIPLY;
            } else if (DivisionRadioButton.Checked)
            {
                return Request.Types.DIVIDE;
            } else if (ExponentRadioButton.Checked)
            {
                return Request.Types.EXPONENT;
            }
            return Request.Types.ADD;
        }
    }
}
