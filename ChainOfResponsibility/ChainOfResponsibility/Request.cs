﻿namespace ChainOfResponsibility
{
    public class Request
    {
        public int Parameter1 { get; internal set; }
        public int Parameter2 { get; internal set; }
        public enum Types {ADD,MULTIPLY,DIVIDE,SUBTRACT,EXPONENT};
        public Types Type{ get; internal set; }

        public Request (int a, int b, Types type)
        {
            Parameter1 = a;
            Parameter2 = b;
            Type = type;
        }
    }
}
