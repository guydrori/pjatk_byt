
public class LineWriter {
	protected static void writeLine(String[] fields) {
		if (fields.length == 0)
			System.out.println();
		else {
			FieldWriter.writeField(fields[0]);

			for (int i = 1; i < fields.length; i++) {
				System.out.print(",");
				FieldWriter.writeField(fields[i]);
			}
			System.out.println();
		}
	}
}
