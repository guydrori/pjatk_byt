
public class FieldWriter {
	protected static void writeField(String field) {
		if (field.indexOf(',') != -1 || field.indexOf('\"') != -1)
			QuoteWriter.writeQuoted(field);
		else
			System.out.print(field);
	}
}
