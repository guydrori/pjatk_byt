/* Bad Smell: Long parameter list
 * Refactoring technique applied: Preserve Whole Object
*/
public class Matcher {
	private int _clipLimit;
	private int _delta;
	
	public Matcher(int clipLimit, int delta) {
		_clipLimit = clipLimit;
		_delta = delta;
	}

	public boolean match(int[] expected, int[] actual) {

		// Clip "too-large" values
		for (int i = 0; i < actual.length; i++)
			if (actual[i] > _clipLimit)
				actual[i] = _clipLimit;

		// Check for length differences
		if (actual.length != expected.length)
			return false;

		// Check that each entry within expected +/- delta
		for (int i = 0; i < actual.length; i++)
			if (Math.abs(expected[i] - actual[i]) > _delta)
				return false;

		return true;
	}
}