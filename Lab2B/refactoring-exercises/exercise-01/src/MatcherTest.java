import static org.junit.Assert.*;

import org.junit.Test;


public class MatcherTest {

	@Test
	public void testMatch() {
		int clipLimit = 100;
		int delta = 5;
		Matcher matcher = new Matcher(clipLimit,delta);

		int[] expected = new int[] { 10, 50, 30, 98 };

		int[] actual = new int[] { 12, 55, 25, 110 };

		assertTrue(matcher.match(expected, actual));

		actual = new int[] { 10, 60, 30, 98 };
		assertTrue(!matcher.match(expected, actual));

		actual = new int[] { 10, 50, 30 };
		assertTrue(!matcher.match(expected, actual));
	}
}
