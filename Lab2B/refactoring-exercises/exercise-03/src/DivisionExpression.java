
public class DivisionExpression extends Expression{
	
	public DivisionExpression(Expression left, Expression right) {
		super(left,right);
	}
	
	public int evaluate() {
		return left.evaluate() / right.evaluate();
	}
}
