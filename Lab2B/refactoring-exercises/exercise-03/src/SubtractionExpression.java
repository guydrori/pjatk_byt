
public class SubtractionExpression extends Expression{
	
	public SubtractionExpression(Expression left, Expression right) {
		super(left,right);
	}
	
	public int evaluate() {
		return left.evaluate() - right.evaluate();
	}
}
