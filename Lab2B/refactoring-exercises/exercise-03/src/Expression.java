/* Bad Smell: Switch statement
 * Refactoring technique applied: Replace Type Code with Subclasses
*/
public abstract class Expression {

	protected Expression left;

	protected Expression right;
	
	public Expression(Expression left,Expression right) {
		this.left = left;
		this.right = right;
	}

	public abstract int evaluate();
}
