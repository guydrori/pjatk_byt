import static org.junit.Assert.*;

import org.junit.Test;

public class ExpressionTest {

	@Test
	public void testConstant() {
		Expression e = new ConstantExpression(-43);
		assertEquals(e.evaluate(), -43);
	}
	@Test
	public void testAddition() {
		Expression e = new AdditionExpression(new ConstantExpression(100), new ConstantExpression(-100));
		assertEquals(e.evaluate(), 0);
	}
	@Test
	public void testSubtraction() {
		Expression e = new SubtractionExpression(new ConstantExpression(100), new ConstantExpression(
				-100));
		assertEquals(e.evaluate(), 200);
	}
	@Test
	public void testMultiplication() {
		Expression e = new MultiplicationExpression( new ConstantExpression(100), new ConstantExpression(
				-100));
		assertEquals(e.evaluate(), -10000);
	}
	@Test
	public void testDivision() {
		Expression e = new DivisionExpression(new ConstantExpression(100), new ConstantExpression(
				-100));
		assertEquals(e.evaluate(), -1);
	}
	@Test
	public void testComplexExpression() {
		// 1+2-3*4/5
		Expression e = new SubtractionExpression(new AdditionExpression(
				new ConstantExpression(1), new ConstantExpression(2)), new DivisionExpression(
				new MultiplicationExpression(new ConstantExpression(3), new ConstantExpression(4)),
				new ConstantExpression(5)));
		assertEquals(e.evaluate(), 1);
	}

}
