
public class MultiplicationExpression extends Expression{
	
	public MultiplicationExpression(Expression left, Expression right) {
		super(left,right);
	}
	
	public int evaluate() {
		return left.evaluate() * right.evaluate();
	}
}
