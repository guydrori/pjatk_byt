
public class AdditionExpression extends Expression{
	
	public AdditionExpression(Expression left, Expression right) {
		super(left,right);
	}
	
	public int evaluate() {
		return left.evaluate() + right.evaluate();
	}
}
