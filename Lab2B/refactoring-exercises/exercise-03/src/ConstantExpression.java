
public class ConstantExpression extends Expression  {
	private int _constant;
	
	public ConstantExpression(int constant) {
		super(null,null);
		_constant = constant;
	}

	@Override
	public int evaluate() {
		// TODO Auto-generated method stub
		return _constant;
	}
}
