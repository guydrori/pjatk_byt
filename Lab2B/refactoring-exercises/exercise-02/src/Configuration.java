/* Bad Smell: Duplicate code
 * Extract Method
*/
import java.util.*;

public class Configuration {
	public int interval;

	public int duration;

	public int departure;

	public void load(Properties props) throws ConfigurationException {
		interval = getValue(props,"interval");

		duration = getValue(props, "duration");

		departure = getValue(props,"departure");
	}

	private int getValue(Properties props,String propertyString) throws ConfigurationException {
		String valueString = props.getProperty(propertyString);
		if (valueString == null) {
			throw new ConfigurationException(propertyString + " offset");
		}
		int value = Integer.parseInt(valueString);
		if (value <= 0) {
			throw new ConfigurationException(propertyString + " > 0");
		}
		if (!propertyString.equals("interval")) {
			if ((value % interval) != 0) {
				throw new ConfigurationException(propertyString + " % interval");
			}
		}
		return value;
	}
}
