/* Bad Smells: Long Method, Duplicate Code
 * Refactoring technique applied: Extract Method
*/
public class TicTacToe {
	public StringBuffer board;

	public TicTacToe(String s) {
		board = new StringBuffer(s);
	}
	
	public TicTacToe(String s, int position, char player) {
		board = new StringBuffer(s);
		board.setCharAt(position, player);
	}
	
	public int suggestMove(char player) {
		for (int i = 0; i < 9; i++) {
			if (isEmpty(i)) {
				TicTacToe game = makeMove(i, player);
				if (game.winner() == player)
					return i;
			}
		}

		for (int i = 0; i < 9; i++) {
			if (isEmpty(i))
				return i;
		}

		return -1;
	}

	private boolean isEmpty(int position) {
		return board.charAt(position) == '-';
	}

	public TicTacToe makeMove(int i, char player) {
		return new TicTacToe(board.toString(), i, player);
	}

	public char winner() {
		// check for horizontal winner
		char horizontalWinner = getHorizontalWinner();

		// check for vertical winner
		char verticalWinner = getVerticalWinner();

		// check for diagonal winner
		char diagonalWinner = getDiagonalWinner();

		// no winner yet
		if (horizontalWinner != Character.MIN_VALUE) return horizontalWinner;
		else if (verticalWinner != Character.MIN_VALUE) return verticalWinner;
		else if (diagonalWinner != Character.MIN_VALUE) return diagonalWinner;
		else return '-';
	}

	private char getDiagonalWinner() {
		if (board.charAt(0) != '-' && board.charAt(4) == board.charAt(0)
				&& board.charAt(8) == board.charAt(0))
			return board.charAt(0);
		if (board.charAt(2) != '-' && board.charAt(4) == board.charAt(2)
				&& board.charAt(6) == board.charAt(2))
			return board.charAt(2);
		return Character.MIN_VALUE;
	}

	private char getVerticalWinner() {
		for (int i = 0; i < 3; ++i) {
			if (board.charAt(i) != '-'
					&& board.charAt(i + 3) == board.charAt(i)
					&& board.charAt(i + 6) == board.charAt(i))
				return board.charAt(i);
		}
		return Character.MIN_VALUE;
	}

	private char getHorizontalWinner() {
		for (int i = 0; i < 9; i += 3) {
			if (board.charAt(i) != '-'
					&& board.charAt(i + 1) == board.charAt(i)
					&& board.charAt(i + 2) == board.charAt(i))
				return board.charAt(i);
		}
		return Character.MIN_VALUE;
	}
}
